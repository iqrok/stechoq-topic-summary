<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * block topic summary.
 *
 * @package    block_stechoq_topic_summary
 * @copyright  IqroK <blogo3x@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../../config.php');
require_once(dirname(__FILE__) . '/lib/activity_report.lib.php');

global $OUTPUT, $PAGE;

date_default_timezone_set("Asia/Jakarta");

function display($content){
	global $OUTPUT;

	echo $OUTPUT->header();
	echo $content;
	echo $OUTPUT->footer();
}

// Check for all required variables.
$sectionId = required_param('id', PARAM_INT);
$courseId = required_param('course', PARAM_INT);
$instanceId = required_param('instance', PARAM_INT);

if (!$course = $DB->get_record('course', array('id' => $courseId))) {
	print_error('invalidcourse', 'block_stechoq_topic_summary', $courseId);
}

require_login($course);

$report = new ActivityReport($instanceId);
$report->hidden_activities_add('daily');

$details = $report->get_section_details($sectionId);
$title = 'Report : ' . $details->name . ' [' . $details->course->name . ']';

$PAGE->set_pagelayout('standard');
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_url(
		'/blocks/stechoq_topic_summary/summary.php',
		array('id' => $sectionId, 'course' => $courseId)
	);

if(isset($_GET['excel']) && $_GET['excel'] == 1){
	display($report->fetch($courseId, $sectionId)->excel());
	die();
}

$excel_href = $report->is_valid_activity($courseId, $sectionId)
	? '<div class="col-3 float-right"><form method="GET" target="_blank">'
			. '<input type="hidden" name="id" value="'.$sectionId.'">'
			. '<input type="hidden" name="course" value="'.$courseId.'">'
			. '<input type="hidden" name="instance" value="'.$instanceId.'">'
			. '<input type="hidden" name="excel" value="1">'
			. '<input type="submit" class="btn btn-block btn-primary"'
				.' value="Unduh Excel">'
		. '</form></div>'
	: '<div class="col-12">'
		. '<div class="alert alert-danger text-center">'
			. '<span class="h1 text-danger">'
				. '<i class="fa fa-lg fa-exclamation-circle"></i>'
				. ' Topic Has No Activity!'
			.'</span>'
		. '</div></div>';

$table = $report->fetch($courseId, $sectionId)->table();
$html = '<div class="row justify-content-end">'
	. $excel_href
	. '</div>'
	. $table;

display($html);
