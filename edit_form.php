<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * block topic summary.
 *
 * @package    block_stechoq_topic_summary
 * @copyright  IqroK <blogo3x@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_stechoq_topic_summary_edit_form extends block_edit_form {

    protected function specific_definition($mform) {

        // Section header title according to language file.
        $mform->addElement('header', 'configheader', 'Percentage');

        // Add Assigments percentage
        $mform->addElement('text', 'config_percentage_assignments', 'Assignment (%)');
        $mform->setDefault(
				'config_percentage_assignments',
				get_string('config_percentage_assignments', 'block_stechoq_topic_summary')
			);
        $mform->setType('config_percentage_assignments', PARAM_NUMBER);

        $mform->addElement('text', 'config_percentage_quiz', 'Quiz (%)');
        $mform->setDefault(
				'config_percentage_quiz',
				get_string('config_percentage_quiz', 'block_stechoq_topic_summary')
			);
        $mform->setType('config_percentage_quiz', PARAM_NUMBER);

        $mform->addElement('text', 'config_percentage_test', 'Test (%)');
        $mform->setDefault(
				'config_percentage_test',
				get_string('config_percentage_test', 'block_stechoq_topic_summary')
			);
        $mform->setType('config_percentage_test', PARAM_NUMBER);
    }
}
