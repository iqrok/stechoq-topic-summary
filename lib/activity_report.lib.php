<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * block topic summary.
 *
 * @package    block_stechoq_topic_summary
 * @copyright  IqroK <blogo3x@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $DB;

require_once(dirname(__FILE__) . '/PHPSpreadsheet/autoload.php');

use \PhpOffice\PhpSpreadsheet\Style\Alignment as _PX_ALIGNMENT;
use \PhpOffice\PhpSpreadsheet\Style\Border as _PX_BORDER;
use \PhpOffice\PhpSpreadsheet\Style\NumberFormat as _PX_NUMBER;
use \PhpOffice\PhpSpreadsheet\Style\Fill as _PX_FILL;
use \PhpOffice\PhpSpreadsheet\Shared\Date as _PX_DATE;

class ActivityReport {
	private $activities;
	private $students;
	private $report;
	private $section;
	private $block;
	private $config;

	private $activity_types;
	private $activity_type_title_prefix;
	private $activity_type_identifier;
	private $activity_list;
	private $activity_columns;

	private $hidden_activities;

	function __construct($instanceId = null){
		$this->hidden_activities = array();
		$this->activity_types = array(
				'daily' => array(
						'prefix' => 'Hari',
						'identifier' => null,
					),

				'assignments' => array(
						'prefix' => 'Tugas',
						'identifier' => 'TUGAS',
					),

				'quiz' => array(
						'prefix' => 'Kuis',
						'identifier' => '(KUIS|QUIZ)',
					),

				'test' => array(
						'prefix' => 'Ujian',
						'identifier' => '(UJIAN|UTS|UAS)',
					),
			);

		$this->init();

		if($instanceId != null){
			$this->get_block_instance($instanceId);
		}
	}

	private function set_percentages(){
		if(empty($this->config)){
			$this->config = new stdClass;
		}

		if(empty($this->config->percentages)){
			$this->config->percentages = new stdClass;
		}

		foreach($this->activity_types as $key => $val){
			$index = 'percentage_' . $key;
			$this->config->percentages->{$key} = $this->config->{$index} / 100;
		}
	}

	private function get_block_instance($id){
		global $DB;

		$instance = $DB->get_record('block_instances', array('id' => $id));
		$this->block = block_instance($instance->blockname, $instance);

		// set config from block's configuration
		$this->config = $this->block->config;
		$this->set_percentages();
	}

	private function init(){
		$this->report = null;
		$this->students = null;
		$this->activities = null;

		$this->activity_type_title_prefix = array();
		$this->activity_type_identifier = array();
		$this->activity_list = array();
		$this->activity_columns = array();

		foreach($this->activity_types as $key => $item){
			if($this->hidden_activities_is_active($key)){
				continue;
			}

			$this->activity_list[$key] = array();
			$this->activity_columns[$key] = array( 'min' => 9999, 'max' => -1 );
			$this->activity_type_title_prefix[$key] = $item['prefix'];

			if($item['identifier']){
				$this->activity_type_identifier[$key] = $item['identifier'];
			}
		}
	}

	public function hidden_activities_add($name){
		array_push($this->hidden_activities, $name);

		// re-init data
		$this->init();
	}

	public function hidden_activities_clear($name){
		$this->hidden_activities = array();
	}

	public function hidden_activities_is_active($name){
		return in_array($name, $this->hidden_activities);
	}

	public function get_section_details($sectionId){
		global $DB;

		$sql = "SELECT
				sect.id,
				sect.name,
				crs.id courseid,
				crs.shortname coursename
			FROM cocoon_course_sections sect
			JOIN cocoon_course crs ON sect.course = crs.id
			WHERE sect.id = ?;";

		$result = $DB->get_record_sql($sql, [ $sectionId ]);

		$ret = new stdClass;
		$ret->id = $result->id;
		$ret->name = $result->name;
		$ret->course = new stdClass;
		$ret->course->id = $result->courseid;
		$ret->course->name = $result->coursename;

		return $ret;
	}

	private function get_enrolled_students($courseId, $activities){
		global $DB;

		$sql = "SELECT
				usr.id,
				usr.username,
				CONCAT(usr.firstname, ' ', usr.lastname) fullname,
				usr.idnumber
			FROM {user} usr
			JOIN {role_assignments} rolas ON rolas.userid = usr.id
			JOIN {context} ctx ON ctx.id = rolas.contextid
			JOIN {course} crs ON crs.id = ctx.instanceid
			JOIN {role} role ON role.id = rolas.roleid AND role.id = 5
			JOIN {course_categories} ccat ON ccat.id = crs.category
			WHERE crs.id = ?
			ORDER BY fullname ASC";

		$students = $DB->get_records_sql($sql, [ $courseId ]);
		$list = array();
		foreach($students as $item){
			$list[$item->id] = $item;
			$list[$item->id]->activities = $activities;
		}

		return $list;
	}

	private function determine_assign_quiz_type($name, $types, $default){
		$ret = $default;

		foreach($types as $type => $title){
			if(preg_match("/^\[".$title."\]/i", trim($name))){
				$ret = $type;
			}
		}

		return $ret;
	}

	private function list_activities($courseId, $sectionId){
		global $DB;

		$sql = "SELECT
				cmdl.id cmid
			FROM {course_sections} sect
			JOIN {course} crs ON sect.course = crs.id
			JOIN {course_modules} cmdl ON cmdl.course = sect.course
				AND cmdl.section = sect.id
			JOIN {modules} mdl ON cmdl.module = mdl.id
			WHERE sect.id = ? AND crs.id = ?
			ORDER BY sect.course, sect.id, LOCATE(cmdl.id, sect.sequence)";
		$ids = $DB->get_records_sql($sql, [ $sectionId, $courseId ]);

		$activities = $this->activity_list;

		$types = $this->activity_type_identifier;

		$modinfo = get_fast_modinfo($courseId);
		foreach ($ids as $item) {
			$cm = $modinfo->get_cm($item->cmid);

			$details = array(
					'id' => $item->cmid,
					'instance' => $cm->instance,
					'name' => $cm->name,
					'module' => array(
						'id' => $cm->module,
						'name' => $cm->modname,
					),
				);

			if($cm->availability){
				$restriction = json_decode($cm->availability, true);

				foreach($restriction['c'] as $rule){
					if($rule['type'] == 'date'){
						if($rule['d'] == '>=' || $rule['d'] == '>'){
							$details['start'] = $rule['t'];
						} else if($rule['d'] == '<=' || $rule['d'] == '<'){
							$details['finish'] = $rule['t'];
						}
					}
				}
			}

			if($cm->modname == 'assign'){
				$details['details'] = $this->get_assignment_details($cm);
				$type = $this->determine_assign_quiz_type(
						$details['name'],
						$types,
						'assignments'
					);

				$activities[$type][$item->cmid] = $details;
			} else if($cm->modname == 'quiz'){
				$details['details'] = $this->get_assignment_details($cm);
				$type = $this->determine_assign_quiz_type(
						$details['name'],
						$types,
						'quiz'
					);

				$activities[$type][$item->cmid] = $details;
			} else {
				$activities['daily'][$item->cmid] = $details;
			}
		}

		return $activities;
	}

	private function get_assignment_details($cm){
		switch($cm->modname){
			case 'quiz':
				return $this->get_quiz_details($cm->instance);
				break;

			case 'assign':
				return $this->get_assign_details($cm->instance);
				break;

			default:
				break;
		}

		return false;
	}

	private function get_assign_details($instance){
		global $DB;

		$sql = "SELECT
				assign.id,
				assign.name,
				assign.course,
				assign.duedate timeclose,
				assign.grade
			FROM {assign} assign
			WHERE assign.id = ?
			LIMIT 1";

		$result = $DB->get_record_sql($sql, [ $instance ]);

		if(!$result){
			return result;
		}

		$parsed = new stdClass;
		$parsed->id = $result->id;
		$parsed->name = $result->name;

		$parsed->time = new stdClass;
		$parsed->time->close = $result->timeclose;

		$parsed->grades = new stdClass;
		$parsed->grades->max = $result->grade;

		return $parsed;
	}

	private function get_quiz_details($instance){
		global $DB;

		$sql = "SELECT
				quiz.id,
				quiz.name,
				quiz.course,
				quiz.timeopen,
				quiz.timeclose,
				quiz.timelimit,
				quiz.sumgrades,
				quiz.grade
			FROM {quiz} quiz
			WHERE quiz.id = ?
			LIMIT 1";

		$result = $DB->get_record_sql($sql, [ $instance ]);

		if(!$result){
			return false;
		}

		$parsed = new stdClass;
		$parsed->id = $result->id;
		$parsed->name = $result->name;

		$parsed->time = new stdClass;
		$parsed->time->open = $result->timeopen;
		$parsed->time->close = $result->timeclose;
		$parsed->time->limit = $result->timelimit;

		$parsed->grades = new stdClass;
		$parsed->grades->sum = $result->sumgrades;
		$parsed->grades->max = $result->grade;

		return $parsed;
	}

	private function get_student_assignment_grade($userId, $assignment){
		switch($assignment['module']['name']){
			case 'quiz':
				return $this->get_quiz_grade($userId, $assignment['details']);
				break;

			case 'assign':
				return $this->get_assign_grade($userId, $assignment['details']);
				break;

			default:
				break;
		}

		return false;
	}

	private function get_assign_grade($userId, $assign){
		global $DB;

		$sql = "SELECT
				agr.timemodified timestamp,
				agr.grade
			FROM {assign_grades} agr
			WHERE
				agr.grader != -1
				AND agr.userid = ?
				AND agr.assignment = ?
			LIMIT 1";

		$result = $DB->get_record_sql($sql, [ $userId, $assign->id ]);

		if($result){
			$result->timeformat = date('Y-m-d', $result->timestamp);
			$result->max = $assign->grades->max;
			$result->percent = 100 * $result->grade / $result->max;

			return $result;
		}

		$result = new stdClass;
		$result->timestamp = null;
		$result->grade = 0;
		$result->max = $assign->grades->max;
		$result->percent = 0;

		return $result;
	}

	private function get_quiz_grade($userId, $quiz){
		global $DB;

		$sql = "SELECT
				qgr.timemodified timestamp,
				qgr.grade
			FROM {quiz_grades} qgr
			WHERE qgr.userid = ?
				AND qgr.quiz = ?
			LIMIT 1";

		$result = $DB->get_record_sql($sql, [ $userId, $quiz->id ]);

		if($result){
			$result->max = $quiz->grades->sum;
			$result->percent = 100 * $result->grade / $result->max;

			return $result;
		}

		$result = new stdClass;
		$result->timestamp = null;
		$result->grade = 0;
		$result->max = $quiz->grades->sum;
		$result->percent = 0;

		return $result;
	}

	private function get_student_activity_completion($userId, $cmId){
		global $DB;

		$sql = "SELECT
				cmpl.completionstate status,
				cmpl.viewed,
				cmpl.timemodified timestamp
			FROM {course_modules_completion} cmpl
			WHERE cmpl.userid = ? AND cmpl.coursemoduleid = ?";

		$result = $DB->get_record_sql($sql, [ $userId, $cmId ]);

		if($result == false){
			$ret = new stdClass;
			$ret->status = 0;
			$ret->viewed = null;
			$ret->timestamp = null;

			return $ret;
		}

		return $result;
	}

	private function get_completion_status($activities, $students){
		$types = array(
				'assignments',
				'quiz',
				'test',
			);

		foreach($students as $student){
			foreach($activities['daily'] as $daily){
				$id = $daily['id'];
				$result = $this->get_student_activity_completion(
						$student->id,
						$daily['id']
					);

				$result->timeformat = $result->status
						? date('Y-m-d', $result->timestamp)
						: '';

				$students[$student->id]
					->activities['daily'][$id]['result'] = $result;
			}

			foreach($types as $type){
				foreach($activities[$type] as $assignment){
					$id = $assignment['id'];
					$result = $this->get_student_assignment_grade(
							$student->id,
							$assignment
						);

					$students[$student->id]
						->activities[$type][$id]['result'] = $result;
				}
			}
		}

		return $students;
	}

	private function build_report(){
		$this->report = array();
		$types = $this->activity_type_title_prefix;

		$column = 0;
		$row = 0;

		// header
		$this->report[$row] = array();

		$this->report[$row][$column++] = [
				'value' => 'ID',
				'type' => 'identity',
				'colWidth' => 100,
			];

		$this->report[$row][$column++] = [
				'value' => 'Nama',
				'type' => 'identity',
				'colWidth' => 175,
			];

		foreach($types as $tName => $tTitle){
			if($this->hidden_activities_is_active($tName)){
				continue;
			}

			$nth = 1;
			foreach($this->activities[$tName] as $key => $item){

				$this->report[$row][$column++] = [
						'value' => $tTitle . ' ' . $nth++,
						'name' => $item['name'],
						'timestamp' => is_numeric($item['start'])
							? $item['start']
							: time(),
						'date' => is_numeric($item['start'])
							? date('Y-m-d', $item['start'])
							: '',
						'type' => $tName,
					];
			}
		}

		$row++;
		$column = 0;

		// body
		foreach($this->students as $userId => $student){
			$this->report[$row][$column++] = [
					'value' => $student->username,
					'type' => 'identity',
				];

			$this->report[$row][$column++] = [
					'value' => $student->fullname,
					'type' => 'identity',
				];

			foreach($types as $tName => $tTitle){
				if($this->hidden_activities_is_active($tName)){
					continue;
				}

				foreach($student->activities[$tName] as $key => $item){
					$this->report[$row][$column++] = [
							'value' => is_numeric($item['result']->percent)
								? $item['result']->percent
								: $item['result']->timeformat,
							'date' => $item['result']->timeformat,
							'type' => $tName,
						];
				}
			}

			$row++;
			$column = 0;
		}
	}

	private function excel_col($col_number, $offset = 1){
		$alphabet_length = 26;
		$start_char = 0x41;
		$col_name = '';
		$col_number += $offset;

		while($col_number > 0){
			$modulo = ($col_number - 1) % $alphabet_length;
			$col_name = chr($start_char + $modulo) . $col_name;
			$col_number = intdiv($col_number - $modulo, $alphabet_length);
		}

		return $col_name;
	}

	private function extra_header_column($sheet, $hCol, $hRow, $value,
										$merged = true, $number_format = null){

		$sheet->SetCellValue($this->excel_col($hCol) . $hRow , $value);

		if(is_string($number_format)){
			$sheet->getStyle($this->excel_col($hCol) . $hRow)
				->getNumberFormat()
				->setFormatCode($number_format);
		}

		if($merged){
			$sheet->mergeCells(
					$this->excel_col($hCol) . $hRow
					. ':'
					. $this->excel_col($hCol + 1) . $hRow
				);
		}
	}

	function fetch($courseId, $sectionId){
		$this->section = $this->get_section_details($sectionId);
		$this->activities = $this->list_activities($courseId, $sectionId);
		$this->students = $this->get_enrolled_students(
				$courseId,
				$this->activities
			);

		$this->get_completion_status($this->activities, $this->students);
		$this->build_report();

		return $this;
	}

	private function set_sheet_top_title($sheet, $title){
		$sheet->SetCellValue($title['start'], $title['value']);

		$sheet->mergeCells($title['coords']);

		$sheet->getStyle($title['coords'])
			->getFont()
			->setSize($title['size']);

		$sheet->getStyle($title['coords'])
			->getFont()
			->setBold($title['styles']['bold']);

		$sheet->getStyle($title['coords'])
			->applyFromArray($title['styles']['align']);

		if(preg_match('/([0-9]+)$/g', $title['start'], $matches)){
			$sheet->getRowDimension($matches[0])
				->setRowHeight($title['size'] * 1.2);
		}
	}

	public function excel(){
		if($this->report == null){
			return false;
		}

		$sectionCols = $this->activity_columns;

		$length = count($this->report);
		$offset = array(
				'row' => 5,
				'col' => 0,
			);
		$styles = [
			'alignCenter' => array(
					'alignment' => array(
					   'horizontal' => _PX_ALIGNMENT::HORIZONTAL_CENTER,
					   'vertical' => _PX_ALIGNMENT::VERTICAL_CENTER,
					)
				),

			'alignLeft' => array(
					'alignment' => array(
					   'horizontal' => _PX_ALIGNMENT::HORIZONTAL_LEFT,
					   'vertical' => _PX_ALIGNMENT::VERTICAL_CENTER,
					)
				),

			'borderThin' => array(
					'borders' => array(
						'allBorders' => array(
							'borderStyle' => _PX_BORDER::BORDER_THIN,
							'color' => array('rgb' => '333333')
						)
					)
				),
		];

		$excel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$excel->setActiveSheetIndex(0);

		$sheet = $excel->getActiveSheet();

		// sheet title can only contain max 31 characters
		$sheet->setTitle(
				substr(
					str_replace(
							$sheet->getInvalidCharacters(),
							'',
							$this->section->name
						),
					0,
					31
				)
			);

		// header
		$hCol = 0;
		$hRow = $offset['row'];
		foreach($this->report[0] as $key => $item){
			$incCol = 1;

			$sheet->SetCellValue(
					$this->excel_col($hCol) . $hRow ,
					$item['value']
				);

			if($sectionCols[$item['type']]['min'] > $hCol){
				$sectionCols[$item['type']]['min'] = $hCol;
			}

			if($sectionCols[$item['type']]['max'] < $hCol){
				$sectionCols[$item['type']]['max'] = $hCol;
			}

			if($item['type'] == 'daily'){
				$cellVal = strlen($item['date']) > 0
					? _PX_DATE::PHPToExcel($item['date'])
					: '-';

				$sheet->SetCellValue(
						$this->excel_col($hCol) . ($hRow + 1),
						$cellVal
					);

				$sheet->getStyle($this->excel_col($hCol) . ($hRow + 1))
					->getNumberFormat()
					->setFormatCode(_PX_NUMBER::FORMAT_DATE_YYYYMMDD2);

				$sheet->mergeCells(
						$this->excel_col($hCol) . $hRow
						. ':'
						. $this->excel_col($hCol + 1) . $hRow
					);

				$sheet->mergeCells(
						$this->excel_col($hCol) . ($hRow + 1)
						. ':'
						. $this->excel_col($hCol + 1) . ($hRow + 1)
					);

				$sheet->getColumnDimension($this->excel_col($hCol))
					->setWidth(80, 'px');

				$incCol = 2;
			} else {
				$sheet->mergeCells(
						$this->excel_col($hCol) . $hRow
						. ':'
						. $this->excel_col($hCol) . ($hRow + 1)
					);
			}

			if(is_numeric($item['colWidth'])){
				$sheet->getColumnDimension($this->excel_col($hCol))
					->setWidth($item['colWidth'], 'px');
			}

			$hCol += $incCol;
		}

		// body
		for($idx = 1; $idx < $length; $idx++){
			$col = $offset['col'];
			$row = $offset['row'] + $idx + 1;

			foreach($this->report[$idx] as $key => $item){
				$incCol = 1;

				$sheet->SetCellValue(
						$this->excel_col($col) . $row ,
						$item['value']
					);

				if($item['type'] == 'daily'){
					$cellVal = strlen($item['date']) > 0
						? _PX_DATE::PHPToExcel($item['date'])
						: '-';

					$sheet->SetCellValue(
							$this->excel_col($col) . $row ,
							$cellVal
						);

					$sheet->getStyle($this->excel_col($col) . $row)
						->getNumberFormat()
						->setFormatCode(_PX_NUMBER::FORMAT_DATE_YYYYMMDD2);

					$present_score = 'MAX(0,10 - (('
						. $this->excel_col($col) . $row
						. '-'
						. $this->excel_col($col) . '$' . ($hRow + 1)
						. ') * 1 ))';

					$is_number = 'AND('
							. 'ISNUMBER('
								. $this->excel_col($col) . '$' . ($hRow + 1)
							. '),'
							. 'ISNUMBER('
								. $this->excel_col($col) . '$' . $row
							. ')'
						. ')';

					// limit max score to 10
					$sheet->SetCellValue(
							$this->excel_col($col + 1) . $row,
							'=MIN(10,'
								.'IF(' . $is_number . ', '
									. $present_score . ', 0)'
								. ')'
						);

					$incCol = 2;
				} else if($item['type'] === 'identity'){
					$sheet->getStyle($this->excel_col($col) . $row)
						->getAlignment()
						->setWrapText(true);
				}

				$col += $incCol;
			}

			// save string for total score
			$totalScore = '';

			// average daily score
			if(!$this->hidden_activities_is_active('daily')){
				if($idx == 1){
					$sectionCols['daily']['header_col'] = $hCol;

					$this->extra_header_column(
							$sheet,
							$hCol,
							$hRow,
							'Nilai Kehadiran',
							false
						);

					$this->extra_header_column(
							$sheet,
							$hCol,
							($hRow + 1),
							0.10,
							false
						);

					$sheet->getColumnDimension($this->excel_col($hCol))
							->setAutoSize(true);

					$sheet->getStyle($this->excel_col($hCol) . ($hRow + 1))
						->getNumberFormat()
						->setFormatCode(_PX_NUMBER::FORMAT_PERCENTAGE_00);

					$hCol++;
				}

				$avgStr['daily'] = '';
				for(
					$aCol = $sectionCols['daily']['min'];
					$aCol <= $sectionCols['daily']['max'];
					$aCol += 2
				){
					if($aCol != $sectionCols['daily']['min']){
						$avgStr['daily'] .= ',';
					}

					$avgStr['daily'] .= $this->excel_col($aCol + 1) . $row
						. ':' . $this->excel_col($aCol + 1) . $row;
				}

				$avgStr['daily'] = '=AVERAGE(' . $avgStr['daily'] . ')';

				$this->extra_header_column(
						$sheet,
						$col,
						$row,
						$avgStr['daily'],
						false
					);

				$totalScore .= '('
						. $this->excel_col($col) . $row
						. ' * '
						. $this->excel_col($col) . ($hRow + 1)
					. ')';

				$col++;
			}

			// average scores for assignemnt
			if(!$this->hidden_activities_is_active('assignments')){
				$item = $sectionCols['assignments'];

				if($item['max'] > -1 && $item['min'] < 9999){
					if($idx == 1){
						$sectionCols['assignments']['header_col'] = $hCol;

						$this->extra_header_column(
								$sheet,
								$hCol,
								$hRow,
								'Nilai Tugas',
								false
							);

						$this->extra_header_column(
								$sheet,
								$hCol,
								($hRow + 1),
								$this->config->percentages->assignments,
								false
							);

						$sheet->getColumnDimension($this->excel_col($hCol))
							->setAutoSize(true);

						$sheet->getStyle($this->excel_col($hCol) . ($hRow + 1))
							->getNumberFormat()
							->setFormatCode(_PX_NUMBER::FORMAT_PERCENTAGE_00);

						$hCol++;
					}

					$avgStr[$type] = '=AVERAGE('
						. $this->excel_col($item['min']) . $row
						. ':'
						. $this->excel_col($item['max']) . $row
						. ')';

					$this->extra_header_column(
							$sheet,
							$col,
							$row,
							$avgStr[$type],
							false,
							'?0.0?'
						);

					$totalScore .= '+('
							. $this->excel_col($col) . $row
							. ' * '
							. $this->excel_col($col) . ($hRow + 1)
						. ')';

					$col++;
				}
			}

			// average scores for quiz
			if(!$this->hidden_activities_is_active('quiz')){
				$item = $sectionCols['quiz'];

				if($item['max'] > -1 && $item['min'] < 9999){
					if($idx == 1){
						$sectionCols['quiz']['header_col'] = $hCol;

						$this->extra_header_column(
								$sheet,
								$hCol,
								$hRow,
								'Nilai Kuis',
								false
							);

						$this->extra_header_column(
								$sheet,
								$hCol,
								($hRow + 1),
								$this->config->percentages->quiz,
								false
							);

						$sheet->getColumnDimension($this->excel_col($hCol))
							->setAutoSize(true);

						$sheet->getStyle($this->excel_col($hCol) . ($hRow + 1))
							->getNumberFormat()
							->setFormatCode(_PX_NUMBER::FORMAT_PERCENTAGE_00);

						$hCol++;
					}

					$avgStr[$type] = '=AVERAGE('
						. $this->excel_col($item['min']) . $row
						. ':'
						. $this->excel_col($item['max']) . $row
						. ')';

					$this->extra_header_column(
							$sheet,
							$col,
							$row,
							$avgStr[$type],
							false,
							'?0.0?'
						);

					$totalScore .= '+('
								. $this->excel_col($col) . $row
								. ' * '
								. $this->excel_col($col) . ($hRow + 1)
								. ')';

					$col++;
				}
			}

			// average scores for test
			if(!$this->hidden_activities_is_active('test')){
				$item = $sectionCols['test'];

				if($item['max'] > -1 && $item['min'] < 9999){
					if($idx == 1){
						$sectionCols['test']['header_col'] = $hCol;

						$this->extra_header_column(
								$sheet,
								$hCol,
								$hRow,
								'Nilai Ujian',
								false
							);

						$this->extra_header_column(
								$sheet,
								$hCol,
								($hRow + 1),
								$this->config->percentages->test,
								false,
								'?0.0?'
							);

						$sheet->getColumnDimension($this->excel_col($hCol))
							->setAutoSize(true);

						$sheet->getStyle($this->excel_col($hCol) . ($hRow + 1))
							->getNumberFormat()
							->setFormatCode(_PX_NUMBER::FORMAT_PERCENTAGE_00);

						$hCol++;
					}

					$avgStr[$type] = '=AVERAGE('
						. $this->excel_col($item['min']) . $row
						. ':'
						. $this->excel_col($item['max']) . $row
						. ')';

					$this->extra_header_column(
							$sheet,
							$col,
							$row,
							$avgStr[$type],
							false,
							'?0.0?'
						);

					$totalScore .= '+('
							. $this->excel_col($col) . $row
							. ' * '
							. $this->excel_col($col) . ($hRow + 1)
						. ')';

					$col++;
				}
			}

			if($idx == 1){
				$this->extra_header_column(
						$sheet,
						$hCol,
						$hRow,
						'Total',
						false
					);

				$first_header_col = 9999;
				foreach($sectionCols as $key => $item){
					if(
						$item['header_col'] < $first_header_col
						&& $item['header_col'] != null
					){
						$first_header_col = $item['header_col'];
					}
				}

				$this->extra_header_column(
						$sheet,
						$hCol,
						$hRow + 1,
						'=SUM('
							. $this->excel_col($first_header_col)
								. $hRow + 1
							. ':'
							. $this->excel_col($hCol - 1)
								. $hRow + 1
							. ')',
						false
					);

				$sheet->getStyle($this->excel_col($hCol) . ($hRow + 1))
					->getNumberFormat()
					->setFormatCode(_PX_NUMBER::FORMAT_PERCENTAGE_00);

				$sheet->getColumnDimension($this->excel_col($hCol))
					->setWidth(80, 'px');
			}

			$totalScore = '=(' . $totalScore . ')'
				. '/'
				. '$' . $this->excel_col($hCol) . '$' . ($hRow + 1);

			$this->extra_header_column(
					$sheet,
					$col,
					$row,
					$totalScore,
					false,
					'?0.0?'
				);
		}

		// set Course name as First Row Title
		$this->set_sheet_top_title(
				$sheet,
				array(
					'start' => $this->excel_col(0) . (1),
					'finish' => $this->excel_col($hCol) . (1),
					'coords' => $this->excel_col(0) . (1)
						. ':'
						. $this->excel_col($hCol) . (1),
					'size' => 16,
					'value' => $this->section->course->name,
					'styles' => array(
							'align' => $styles['alignLeft'],
							'bold' => true,
						),
				)
			);

		// set Topic name as Second Row Title
		$this->set_sheet_top_title(
				$sheet,
				array(
					'start' => $this->excel_col(0) . (2),
					'finish' => $this->excel_col($hCol) . (2),
					'coords' => $this->excel_col(0) . (2)
						. ':'
						. $this->excel_col($hCol) . (2),
					'size' => 14,
					'value' => $this->section->name,
					'styles' => array(
							'align' => $styles['alignLeft'],
							'bold' => true,
						),
				)
			);

		// set date & time for the third Row
		$this->set_sheet_top_title(
				$sheet,
				array(
					'start' => $this->excel_col(0) . (3),
					'finish' => $this->excel_col($hCol) . (3),
					'coords' => $this->excel_col(0) . (3)
						. ':'
						. $this->excel_col($hCol) . (3),
					'size' => 12,
					'value' => date('d M Y (H:i:s)', time()),
					'styles' => array(
							'align' => $styles['alignLeft'],
							'bold' => false,
						),
				)
			);

		// set header as Bold
		$header = array(
				'coords' => $this->excel_col(0) . $hRow
					. ':'
					. $this->excel_col($hCol) . ($hRow + 1),
			);

		$sheet->getStyle($header['coords'])
			->getFont()
			->setBold(true);

		$sheet->getStyle($header['coords'])
			->getFill()
			->setFillType(_PX_FILL::FILL_SOLID)
			->getStartColor()
			->setARGB('80FF80');

		// align header cells in center
		$body_cells = $this->excel_col(0) . $offset['row']
				. ':'
				. $this->excel_col($hCol) . ($offset['row'] + $idx);

		$sheet->getStyle($body_cells)->applyFromArray($styles['alignCenter']);

		$sheet->getStyle($body_cells)->applyFromArray($styles['borderThin']);

		// align name and id in left
		$sheet->getStyle(
				$this->excel_col(1) . ($hRow + 2)
				. ':'
				. $this->excel_col(1) . ($hRow + $idx)
			)->applyFromArray($styles['alignLeft']);

		// freeze Headers Row And Identity Column
		$sheet->freezePane($this->excel_col(2) . ($hRow + 2));

		// reset report data
		$this->init();

		// create xlsx file
		$filename = '[' . $this->section->course->name . '] '
			. $this->section->name
			. '.xlsx';

		header(
				'Content-Type: '
				. 'application/'
				. 'vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			);
		header(
				'Content-Disposition: attachment;'
				. 'filename="' . trim($filename) . '"'
			);
		header('Cache-Control: max-age=0');

		$xlsxWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter(
				$excel,
				'Xlsx'
			);

		$xlsxWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($excel);

		die($xlsxWriter->save('php://output'));
	}

	function table(){
		if($this->report == null){
			return false;
		}

		$html = new stdClass;
		$html->str = '';
		$html->thead = '';
		$html->tbody = '';
		$html->length = count($this->report);
		$html->title = '<div class="container mt-3">'
			. '<div class="row">'
			. '<div class="col-12 text-center">'
			. '<h1 class="text-center">'
				. $this->section->course->name
			. '</h1>'
			. '<h3 class="text-center">'
				. $this->section->name
			. '</h3>'
			. '</div></div></div>';

		foreach($this->report[0] as $key => $item){
			$html->thead .= '<th class="column-' . $item['type'] . '">'
				. $item['value']
				. '</th>';
		}

		$html->thead = '<thead class="thead-dark"><tr>'
			. $html->thead
			.'</tr></thead>';

		for($idx = 1; $idx < $html->length; $idx++){
			$row = '';

			foreach($this->report[$idx] as $key => $item){
				$value = is_numeric($item['value'])
					? round($item['value'], 2)
					: $item['value'];

				$row .= '<td class="column-' . $item['type'] . '">'
					. $value
					. '</td>';
			}

			$html->tbody .= '<tr>' . $row . '</tr>';
		}
		$html->tbody = '<tbody>' . $html->tbody . '</tbody>';

		$html->str = $html->title
			. '<div style="overflow-x: auto">'
			. '<table class="table table-sm table-bordered table-hover">'
			. $html->thead
			. $html->tbody
			. '</table>'
			. '</div>';

		// reset report data
		$this->init();

		return $html->str;
	}

	public function is_valid_activity($courseId, $sectionId){
		$activities = $this->list_activities($courseId, $sectionId);

		foreach($activities as $key => $item){
			if(is_array($item) && count($item) > 0){
				return true;
			}
		}

		return false;
	}

	public function list_sections($courseId, $_baseurl = ''){
		global $DB;

		$sections = $DB->get_records_sql("SELECT
				sect.id,
				sect.name
			FROM {course_sections} sect
			JOIN {course} crs ON sect.course = crs.id
			WHERE sect.section > 0
				AND sect.visible = 1
				AND crs.id = ?
			ORDER BY sect.course, sect.section", [ $courseId ]);

		if(!$sections){
			print_error('invalidsections', 'block_stechoq_topic_summary', $courseId);
		}

		$html = '<table class="table table-hover">
		<thead class="thead-light">
			<tr>
				<th>Section Title</th>
				<th>Actions</th>
			</tr>
		</thead> <tbody>';
		foreach($sections as $item){
			$target_url = new moodle_url(
					$_baseurl . '/summary.php',
					[
						'id' => $item->id,
						'course' => $courseId ,
						'instance' => $this->block->instance->id,
					]
				);

			$html .= '<tr>
				<td>'. $item->name .'</td>
				<td>'. html_writer::link($target_url, 'View Report') .'</td>
			</tr>';
		}
		$html .= '</tbody></table>';

		return $html;
	}
}

?>
