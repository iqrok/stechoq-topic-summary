<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * jbxl helper.
 *
 * @copyright  Fumi.Iseki
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function  jbxl_get_course_context($courseid){
	$context = context_course::instance($courseid, IGNORE_MISSING);
	return $context;
}

function  jbxl_is_admin($uid){
	$admins = get_admins();
	foreach ($admins as $admin) {
		if ($uid==$admin->id) return true;
	}
	return false;
}

function  jbxl_is_teacher($uid, $context, $inc_admin=true){
	global $DB;

	if (!$context) return false;
	if (!is_object($context)) $context = jbxl_get_course_context($context);

	$ret = false;
	$roles = $DB->get_records('role', array('archetype'=>'editingteacher'), 'id', 'id');
	foreach($roles as $role) {
		$ret = user_has_role_assignment($uid, $role->id, $context->id);
		if ($ret) return $ret;
	}

	if ($inc_admin) {
		$ret = jbxl_is_admin($uid);
		if (!$ret) $ret = jbxl_has_role($uid, $context, 'manager');
		if (!$ret) $ret = jbxl_has_role($uid, $context, 'coursecreator');
	}
	return $ret;
}

function  jbxl_has_role($uid, $context, $rolename){
    global $DB;

    if (!$context) return false;
    if (!is_object($context)) $context = jbxl_get_course_context($context);

    $roles = $DB->get_records('role', array('archetype'=>$rolename), 'id', 'id');
    foreach($roles as $role) {
        $ret = user_has_role_assignment($uid, $role->id, $context->id);
        if ($ret) return $ret;
    }
    return false;
}
