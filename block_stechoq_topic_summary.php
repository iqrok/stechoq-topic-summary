<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * block topic summary.
 *
 * @package    block_stechoq_topic_summary
 * @copyright  IqroK <blogo3x@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/lib/helper.php');
require_once(dirname(__FILE__) . '/lib/activity_report.lib.php');

defined('MOODLE_INTERNAL') || die();

class block_stechoq_topic_summary extends block_base {
	private $_default_percentage;

    function init() {
        $this->title = get_string('block_title', 'block_stechoq_topic_summary');
        $this->_name = 'stechoq_topic_summary';
        $this->_baseurl = '/blocks/' . $this->_name;

		$this->_default_percentage = 20;
    }

    function get_content() {
		global $CFG, $USER;

		$report = new ActivityReport($this->instance->id);

		if ($this->content !== null) {
			return $this->content;
		}

		$courseId = optional_param('course', 0, PARAM_INTEGER);

		if($courseId == 0){
			$courseId = $this->page->course->id;
		}

		$this->content         =  new stdClass;
		$this->content->text   = '';
		$this->content->footer = 'STECHOQ - Topic Summary';

		$context   = jbxl_get_course_context($courseId);
		$isPermitted = jbxl_is_teacher($USER->id, $context);

		if($isPermitted){
			$sections = $report->list_sections($courseId, $this->_baseurl);
			$this->content->text .= $sections;
		} else {
			$this->content->text .= '';
		}

		return $this->content;
    }

	public function config_save($data) {
		// Default behaviour: save all variables as $CFG properties
		// You don't need to override this if you 're satisfied with the above
		foreach ($data as $name => $value) {
			var_dump($data);
			set_config($name, $value);
		}
		return TRUE;
	}

    public function applicable_formats() {
        return array(
				'all' => false,
				'site' => false,
				'site-index' => false,
				'course-view' => true,
				'course-view-social' => false,
				'mod' => false,
			);
    }

	// fill for empty value with the default one
    private function check_empty_percentage(){
		if(empty($this->config->percentage_assignments)){
			$this->config->percentage_assignments = $this->_default_percentage;
		}

		if(empty($this->config->percentage_quiz)){
			$this->config->percentage_quiz = $this->_default_percentage;
		}

		if(empty($this->config->percentage_test)){
			$this->config->percentage_test = $this->_default_percentage;
		}
	}

    public function specialization() {
		if (!isset($this->config)) {
			$this->config = new \stdClass();
		}

		$this->check_empty_percentage();
    }

    public function instance_allow_multiple() { return false; }
    public function has_config() { return true; }
    public function instance_allow_config() { return true; }
    public function cron() { return true; }
}
